# Lab4：给定指令系统的处理器设计

## 实验目的

- 掌握 Vivado 集成开发环境
- 掌握 Verilog 语言
- 掌握 FPGA 编程方法及硬件调试手段
- 深刻理解处理器结构和计算机系统的整体工作原理

## 实验环境

- Vivado 集成开发环境
- 龙芯 Artix-7 实验平台

## 实验内容

根据《计算机体系结构》第五章里 MIPS 指令集的基本实现，设计并实现一个符合实验指令的非流水处理器， 包括 Verilog 语言的实现和 FPGA 芯片的编程实现，要求该处理器可以通过所提供的自动测试环境。

### 指令系统定义

#### 运算指令

（1）加法指令 `ADD rd, rs, rt`

```{image} ../_static/img/labs/instruction/add.png
:width: 90%
:align: center
```

该指令将两个源寄存器内容相加，结果送回目的寄存器的操作。

具体为： **[rd] <- [rs] + [rt]**

（2）减法指令 `SUB rd, rs, rt`

```{image} ../_static/img/labs/instruction/sub.png
:width: 90%
:align: center
```

该指令将两个源寄存器内容相减，结果送回目的寄存器的操作。

具体为： **[rd] <- [rs] - [rt]**

（3）与运算指令 `AND rd, rs, rt`

```{image} ../_static/img/labs/instruction/and.png
:width: 90%
:align: center
```

该指令将两个源寄存器内容作按位与，结果送回目的寄存器。

具体为： **[rd] <- [rs] & [rt]**

（4）或运算指令 `OR rd, rs, rt`

```{image} ../_static/img/labs/instruction/or.png
:width: 90%
:align: center
```

该指令将两个源寄存器内容作按位或，结果送回目的寄存器。

具体为： **[rd] <- [rs] | [rt]**

（5）异或指令 `XOR rd, rs, rt`

```{image} ../_static/img/labs/instruction/xor.png
:width: 90%
:align: center
```

该指令将两个源寄存器内容作按位异或，结果送回目的寄存器。

具体为： **[rd]<-[rs] ^ [rt]**

(6)移位指令 `SLL rd, rt, sa`

```{image} ../_static/img/labs/instruction/sll.png
:width: 90%
:align: center
```

该指令根据sa字段指定的位移量，对寄存器rt的值进行逻辑左移，结果决定目的寄存器的值。

具体为： **[rd] <- [rt] << sa**

```sll r0, r0, 0```的指令字为全0，显然它不修改任何寄存器（因为```r0```恒为0）。这条指令的另一个更常用的名字是```nop```，即“什么都不做的占位用指令”。

#### 访存指令

（1）存数指令 `SW rt, offset(base)`

```{image} ../_static/img/labs/instruction/sw.png
:width: 90%
:align: center
```

该指令将寄存器 `rt`的内容存于主存单元中，对应的地址由16位偏移地址 `offset`经符号拓展加上 `base`的内容生成。

具体操作为： **Mem[[base] + offset] <- [rt]**

（2）取数指令 `LW rt, offset(base)`

```{image} ../_static/img/labs/instruction/lw.png
:width: 90%
:align: center
```

该指令将主存单元中的内容存于寄存器 `rt`，对应的地址由16位偏移地址 `offset`经符号拓展加上 `base`内容生成。

具体操作为： **[rt] <- Mem[[base] + offset]**

#### 转移指令

（1）无条件转移指令 `J target`

```{image} ../_static/img/labs/instruction/j.png
:width: 90%
:align: center
```

该指令改变下一条指令的地址，地址由指令中的26位形式地址```instr_index```左移2位作为低28位，和```NPC```（该指令的地址+4）的高4位拼接而成。

具体操作为： **PC <- (NPC[31:28]) ## (instr_index << 2)**

（2）条件转移（位测试跳转）指令 `BBT rs, bit, offset`

```{image} ../_static/img/labs/instruction/bbt.png
:width: 90%
:align: center
```

判断寄存器```rs```的第```bit```位是否为1，如果是1则执行跳转，否则不跳转。跳转时，16位偏移```offset```扩充为32位，左移2位后与NPC（这条指令的地址+4）相加，作为下一条指令的地址。

具体操作为： **PC <- ([rs][bit] == 1) ? [sign_extend(offset) << 2 + NPC] : NPC**

### 其他指令

（1）条件移动指令 `MOVZ rd, rs, rt`

```{image} ../_static/img/labs/instruction/movz.png
:width: 90%
:align: center
```

该指令根据其中一个源寄存器内容，决定另一个源寄存器的值是否写回目的寄存器。

具体为： **if ([rt] == 0) then [rd] <- [rs]**

(2)比较指令 `CMP rd, rs, rt`

```{image} ../_static/img/labs/instruction/cmp.png
:width: 90%
:align: center
```

该指令比较寄存器```rs```和```rt```的值，将比较结果写入寄存器```rd```：

- 若```[rs]=[rt]```，将```[rd]```第0位置1，否则置0。
- 若```[rs]<[rt]```（有符号比较），将```[rd]```第1位置1，否则置0。
- 若```[rs]<[rt]```（无符号比较），将```[rd]```第2位置1，否则置0。
- 若```[rs]<=[rt]```（有符号比较），将```[rd]```第3位置1，否则置0。
- 若```[rs]<=[rt]```（无符号比较），将```[rd]```第4位置1，否则置0。
- ```[rd]```的```[9:5]```位是```[rd]```的```[4:0]```位按位取反的结果。
- ```[rd]```的```[31:10]```位（剩余部分）直接清0。

## 实验要求

要求根据以上给定的指令系统设计处理器，包括指令格式设计、操作的定义、Verilog语言的实现及 FPGA 编程实现。处理器设计实验要求按指定阶段进行。

### 实验预习

在实验开始前给出处理器的设计方案，设计方案要求包括：

- 指令格式设计
- 处理器结构设计框图（要求精确到信号以及信号的位宽）及功能描述
- 各功能模块结构设计框图及功能描述
- 各模块输入输出接口信号定义（以表格形式给出）

### 完成实验内容

**Verilog 语言实现**：要求采用结构化设计方法，用 Verilog 语言实现处理器的设计。设计包括：

- 各模块的详细设计（包括各模块功能详述，设计方法，Verilog 语言实现等）
- 各模块的功能测试（每个模块作为一个部分，包括测试方案、测试过程和测 试波形等）
- 系统的详细设计（包括系统功能详述，设计方法，Verilog 语言实现等）
- 系统的功能测试（包括系统整体功能的测试方案、测试过程和测试波形等）

## 处理器测试环境

要求使用我们提供的处理器测试环境对CPU进行仿真测试。

### 使用要求

要求每个同学的CPU对外暴露以下接口（信号名称与位数均不可改变）：

```verilog
module cpu(
    input           clk,                // 时钟信号
    input           resetn,             // 低有效复位信号

    output          inst_sram_en,       // 指令存储器读使能
    output[31:0]    inst_sram_addr,     // 指令存储器读地址
    input[31:0]     inst_sram_rdata,    // 指令存储器读出的数据

    output          data_sram_en,       // 数据存储器端口读/写使能
    output[3:0]     data_sram_wen,      // 数据存储器写使能      
    output[31:0]    data_sram_addr,     // 数据存储器读/写地址
    output[31:0]    data_sram_wdata,    // 写入数据存储器的数据
    input[31:0]     data_sram_rdata,    // 数据存储器读出的数据

    // 供自动测试环境进行CPU正确性检查
    output[31:0]    debug_wb_pc,        // 当前正在执行指令的PC
    output          debug_wb_rf_wen,    // 当前通用寄存器组的写使能信号
    output[4:0]     debug_wb_rf_wnum,   // 当前通用寄存器组写回的寄存器编号
    output[31:0]    debug_wb_rf_wdata   // 当前指令需要写回的数据
);

    // TODO

endmodule
```

注意：

- 只允许按照给定的接口格式去设计CPU，不允许更改接口格式
- 实验提供的vivado项目顶层模块```soc_top```中已经例化了一个名为```your_cpu```的```cpu```模块（处于缺失状态），在你实现了```cpu```模块后，将其添加至实验提供的 vivado 项目中即可自动填充```your_cpu```的空缺。**你并不需要关心，更不可以修改项目中的其它模块或IP核**，它们是用于测试你的CPU的。
- 数据存储器和指令存储器都是时钟同步读写的，即，在```*_wen=0``` **且```*_en=1```** 时，```*_rdata```会在下一个时钟上升沿呈现出读出的数据；在```*_wen=1``` **且```*_en=1```** 时，下一个时钟上升沿时```*_wdata```上呈现的数据会被写入存储器。（由于你显然不需要修改指令存储器的内容，我们将指令存储器的写信号去掉了）
- **请务必避免在你的CPU内部使用下降沿触发（如```always@(negedge clk)```）的时序逻辑**。所有信号都应该在上升沿采样，CPU内部使用的所有寄存器都应该是上升沿触发（```always@(posedge clk)```）的。
- **请务必小心处理输出的```debug_wb_rf_wen```信号**，一条指令如果要写回寄存器，那么它必须令```debug_wb_rf_wen```恰好保持**仅一个周期**的高电平。如果```debug_wb_rf_wen```下一个周期仍为高电平，自动测试环境会认为这是下一条指令的写回信号。另外，自动测试环境仅在```debug_wb_rf_wen=1```时启动一次比对，如果你的```debug_wb_rf_wen```经常（甚至一直）都错误地为0甚至Z或X，那么仿真很有可能很长时间都无法结束，这是CPU仿真卡死的主要原因。**另外，如果当前指令试图写回```r0```，你显然不应该修改恒为0的“寄存器”```r0```的值，此时你的```debug_wb_rf_wen```应该为0**。

### 测试用例

针对本实验中的处理器测试环境，使用我们提供的一个简单的测试用例对CPU进行测试，包含了要求的12种指令。

测试所用指令序列如下：

```
0x00:   lw      r1, 4(r0)       // r1=0x114514
0x04:   lw      r2, 8(r0)       // r2=0x1919810
0x08:   add     r3, r1, r2      // r3=0x1a2dd24
0x0c:   sub     r4, r1, r2      // r4=0xfe7fad04
0x10:   and     r5, r1, r2      // r5=0x110010
0x14:   or      r6, r1, r2      // r6=0x191dd14
0x18:   xor     r7, r1, r2      // r7=0x180dd04
0x1c:   movz    r6, r7, r1      // 不写回r6
0x20:   xor     r8, r8, r8      // r8=0
0x24:   movz    r9, r6, r8      // r9=r6=0x191dd14
0x28:   sw      r5, 0(r8)       
0x2c:   j       0xd             // 跳转到0x34
0x30:   sll     r30, r2, 4      // 不执行
0x34:   sll     r31, r1, 5      // r31=0x228a280
0x38:   cmp     r10, r1, r2     // r10=0x3e
0x3c:   bbt     r10, 0, 0x2     // 跳转到0x48 (不跳转)
0x40:   add     r11, r0, r2     // r11=0x1919810
0x44:   bbt     r10, 2, 0x1     // 跳转到0x4c
0x48:   sub     r12, r0, r2     // 不执行
0x4c:   lw      r7, 0(r0)       // r7=r5=0x110010
0x50:   cmp     r8, r2, r1      // r8=0x3e0
0x54:   cmp     r9, r7, r7      // r9=0xd9
0x58:   and     r0, r1, r2      // 不写回r0
0x5c:   xor     r1, r1, r1      // r1=0
0x60:   j       0x18            // 跳转到0x60 (死循环，测试结束)
0x60:   nop
```

测试所用的数据存储器的初值为：地址4处的32位字为```0x114514```，地址8处的32位字为```0x1919810```，其余初始化为全0。

### 仿真

在进行仿真时，若你实现的CPU功能正确，自动测试环境会在控制台打印 **```CORRECT```** ，如图所示：

```{image} ../_static/img/labs/result_lab4/console_correct.png
:width: 90%
:align: center
```

如果CPU输出的```debug_wb_*```信号有误，自动测试环境会在控制台打印错误信息，如图所示：

```{image} ../_static/img/labs/result_lab4/debug_err.png
:width: 90%
:align: center
```

如果你的CPU在超过50000个时钟周期后（约500us）仍未给出下一个为高电平的```debug_wb_rf_wen```信号，这显然意味着CPU实现有误，那么仿真环境会自动终止，在控制台打印错误信息，如图所示：

```{image} ../_static/img/labs/result_lab4/cpu_error.png
:width: 90%
:align: center
```

注意：

- 当发生错误时，你可以根据控制台上输出的“Detected error at XXX ns”在波形中定位到出错的时间点。
- 由于打印提示信息的需要，本实验给出的测试环境在仿真时一般会在运行100~300us后自动停止，尽管你的CPU在仿真的第1us内一般就已经运行完毕了。**请确保仿真运行了足够长的时间**（例如：仿真时设置运行1ms）。
