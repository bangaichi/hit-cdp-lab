# Lab6：处理器下载（二选一）

## 实验目的

- 掌握 Vivado 集成开发环境
- 掌握 Verilog 语言
- 掌握 FPGA 编程方法及硬件调试手段
- 掌握la32r指令集的20条基础指令

## 实验环境

- Vivado 集成开发环境
- 龙芯 Artix-7 实验平台

## 实验内容

1. 阅读并理解`lab6/myCPU/`目录下提供的代码。
2. 完成代码调试。我们提供的代码中加入了若干错误，请大家通过仿真波形的调试修复这些错误，使设计可以通过仿真和上板验证。

提示：不仅仅是`mycpu_top.v`文件，每一个 `.v` 文件都可能存在错误。

### 指令系统定义

本实验要求实现la32r指令集中的20条基础指令。

`add.w` `sub.w` `slt` `sltu` `nor` `and` `or` `xor` `slli_w` `srli_w`
`srai_w` `addi_w` `ld_w` `st_w` `jirl` `b` `bl` `beq` `bne` `lu12i_w`

```{image} ../_static/img/labs/lab5/ins_sys.png
:width: 90%
:align: center
```

完整的指令集手册可以在这里找到：[https://soc.ustc.edu.cn/COD/lab1/src/LA32R_ref.pdf](https://soc.ustc.edu.cn/COD/lab1/src/LA32R_ref.pdf)

### 接口及控制信号

你设计的指令译码器应该符合这样的接口：

| 名称              | 宽度 | 方向 | 描述                   |
| ----------------- | ---- | ---- | ---------------------- |
| clk               | 1    | IN   | 时钟信号               |
| resetn            | 1    | OUT  | 重置信号               |
| inst_sram_we      | 1    | OUT  | 指令RAM写使能          |
| inst_sram_addr    | 32   | OUT  | 指令RAM读写地址        |
| inst_sram_wdata   | 32   | OUT  | 指令RAM写数据          |
| inst_sram_rdata   | 32   | OUT  | 指令RAM读数据          |
| data_sram_we      | 1    | OUT  | 数据RAM写使能          |
| data_sram_addr    | 32   | OUT  | 数据RAM读写地址        |
| data_sram_wdata   | 32   | OUT  | 数据RAM写数据          |
| data_sram_rdata   | 32   | OUT  | 数据RAM读数据          |
| debug_wb_pc       | 32   | OUT  | 写回阶段的PC           |
| debug_wb_rf_we    | 4    | OUT  | 写回阶段的寄存器写使能 |
| debug_wb_rf_wnum  | 5    | OUT  | 写回阶段的写寄存器号   |
| debug_wb_rf_wdata | 32   | OUT  | 写回阶段的写数据       |

### 创建工程目录（非常重要，必须依照以下方法创建工程）

1.打开 Vivado，停留在初始界面，找到最下面的 **`TCL CONSOLE`**。如果不是这样的界面，那就点击最下面 **`TCL CONSOLE`** 的框。

```{image} ../_static/img/labs/lab5/tcl_console.png
:width: 90%
:align: center
```

2.找到 `lab6\soc_verify\soc_dram\run_vivado` 目录，在资源管理器的地址框中复制**绝对路径**。

```{image} ../_static/img/labs/lab5/path.png
:width: 90%
:align: center
```

3.输入 `cd` 到 Vivado 中 `TCL CONSOLE` 的输入框中, 再粘贴第 2 步中的绝对路径。  
**注意！Vivado 中路径需要`/`而不是Windows默认的`\`。一个便捷的方法是在粘贴后删除末尾的字母，然后会自动提示可补全的路径，这个时候按下TAB即可自动补全，同时`\`全部变为`/`**

```{image} ../_static/img/labs/lab5/cd.png
:width: 90%
:align: center
```

4.输入 `source ./create_project.tcl`，按下回车执行。等待工程创建完毕。

```{image} ../_static/img/labs/lab5/create.png
:width: 90%
:align: center
```

## 实验要求
### 实验预习

- 掌握CPU的工作原理，了解实验开发所需的软硬件平台。
- 掌握20种指令运算操作的意义，完成这20种指令的编码。

### 完成实验内容

按照实验内容完成CPU的设计与实现，在**仿真**和**上板**两种环境下通过本实验所提供的自动测试环境，记录运行过程和结果，完成实验报告。

## 自动测试环境

要求使用我们提供的自动测试环境对CPU进行测试，包括仿真测试和上板测试两部分。

### 使用要求

要求每个同学的CPU对外暴露以下接口（信号名称与位数均不可改变）：

```verilog
module mycpu_top(
    input  wire        clk,
    input  wire        resetn,
    // inst sram interface
    output wire        inst_sram_we,
    output wire [31:0] inst_sram_addr,
    output wire [31:0] inst_sram_wdata,
    input  wire [31:0] inst_sram_rdata,
    // data sram interface
    output wire        data_sram_we,
    output wire [31:0] data_sram_addr,
    output wire [31:0] data_sram_wdata,
    input  wire [31:0] data_sram_rdata,
    // trace debug interface
    output wire [31:0] debug_wb_pc,
    output wire [ 3:0] debug_wb_rf_we,
    output wire [ 4:0] debug_wb_rf_wnum,
    output wire [31:0] debug_wb_rf_wdata
);
// TODO

endmodule
```

注意：

- 这是一个单周期CPU，你实现的```mycpu_top```模块必须为**组合逻辑**，请不要在实现时使用阻塞赋值等用于时序逻辑的写法。

### 测试用例

在 `func/obj/test.s` 中可查看。

当你发现仿真出现错误时，一个很好的办法就是打开这个文件，找到出错指令的位置，查看什么指令发生了错误，以及对应的上下文。

此外你还需要知道，如果是跳转或者访存指令发生错误，那么真正发生错误的指令很可能在更早的某条指令。至于为什么会这样，你可以从我们的**对比纠错机制**出发，来思考这个问题。

### 仿真

在进行仿真时，若你实现的CPU功能正确，自动测试环境会在控制台打印 **```PASS```**

若CPU对某条输入的指令执行错误，自动测试环境会在控制台打印错误信息，错误信息包含你的处理器在哪里出现了错误以及正确信息是什么。

```{image} ../_static/img/labs/lab5/console_result.png
:width: 90%
:align: center
```

### 上板测试

你实现的指令译码器通过仿真验证后，需要将设计下载到 Artix-7 FPGA 开发板上验证设计正确性。当所实现的指令译码器功能正确时，灯管和数码管会显示相应的信息。

在FPGA上板验证时其结果正确与否的判断也只有一种方法，func正确的执行行为是：

1. 开始，单色LED全灭，双色LED灯一红一绿，数码管显示全0；
2. 执行过程中，单色LED全灭，双色LED灯一红一绿，数码管高8位和低8位同步累加；
3. 结束时，单色LED全灭，双色LED灯亮两绿，数码管高8位和低8位数值相同，对应测试功能点数目。
4. 如果func执行过程中出错了，则数码管高8位和低8位第一次不同处即为测试出错的功能点编号，且最后的结果是单色LED全亮，双色LED灯亮两红，数码管高8位和低8位数值不同。

```{image} ../_static/img/labs/lab5/fpga.png
:width: 90%
:align: center
```
