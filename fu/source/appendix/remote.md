# 远程实验平台

* 远程实验平台的介绍和使用。

## 连接远程实验平台

* 连接实验室的无线网络。（**密码**为**happyhacking**）
* 使用浏览器访问如下地址：
  > http://192.168.0.11:8000/
* 成功连接远程实验平台，看到如下图所示的登录界面。

```{image} ../_static/img/imgs/img1.png
:width: 100%
:align: center
```

## 使用远程实验平台

* 登录远程实验平台，共计20组用户，用户名为**user1**到**user20**。
* 所有用户密码均为**happyhacking**。

```{image} ../_static/img/imgs/img2.png
:width: 100%
:align: center
```

* 点击登录按钮后，可以看到如下图所示的界面：

```{image} ../_static/img/imgs/img3.png
:width: 100%
:align: center
```

* 开发板分配项无需选择，点击“**上传位流**”选项右侧的“**选择文件**”按钮，选择vivado生成的bit流文件进行上传。

> 注：vivado工程生成的bit流位置一般是***your_project_name***/***your_project_name***.runs/impl_1/***your_top_name***.bit

```{image} ../_static/img/imgs/img4.png
:width: 100%
:align: center
```

* 点击上传并开始，即可进入远程实验板界面，在此界面可以模拟点击、拨动开关等操作进行实验。

```{image} ../_static/img/imgs/img5.png
:width: 100%
:align: center
```

* 向下滑动页面，可以利用vivado的远程jtag连接功能进行远程调试，具体连接方法见页面介绍。

```{image} ../_static/img/imgs/img6.png
:width: 100%
:align: center
```

## 注意事项

* 远程实验平台的引脚绑定、触发方式与实验箱均有所差异，具体来说需要使用新的xdc文件进行约束，并且需要对rst、leds信号等进行取反操作。