# 实验四测试环境说明

处理器设计实验测试环境。

## 测试样例

测试所用的完整指令序列如下。

```assembly
// 处理器设计实验测试

0x00:   lw      r1, 4(r0)       // r1=0x114514
0x04:   lw      r2, 8(r0)       // r2=0x1919810
0x08:   add     r3, r1, r2      // r3=0x1a2dd24
0x0c:   sub     r4, r1, r2      // r4=0xfe7fad04
0x10:   and     r5, r1, r2      // r5=0x110010
0x14:   or      r6, r1, r2      // r6=0x191dd14
0x18:   xor     r7, r1, r2      // r7=0x180dd04
0x1c:   movz    r6, r7, r1      // 不写回r6
0x20:   xor     r8, r8, r8      // r8=0
0x24:   movz    r9, r6, r8      // r9=r6=0x191dd14
0x28:   sw      r5, 0(r8)       
0x2c:   j       0xd             // 跳转到0x34
0x30:   sll     r30, r2, 4      // 不执行
0x34:   sll     r31, r1, 5      // r31=0x228a280
0x38:   cmp     r10, r1, r2     // r10=0x3e
0x3c:   bbt     r10, 0, 0x2     // 跳转到0x48 (不跳转)
0x40:   add     r11, r0, r2     // r11=0x1919810
0x44:   bbt     r10, 2, 0x1     // 跳转到0x4c
0x48:   sub     r12, r0, r2     // 不执行
0x4c:   lw      r7, 0(r0)       // r7=r5=0x110010
0x50:   cmp     r8, r2, r1      // r8=0x3e0
0x54:   cmp     r9, r7, r7      // r9=0xd9
0x58:   and     r0, r1, r2      // 不写回r0
0x5c:   xor     r1, r1, r1      // r1=0
0x60:   j       0x18            // 跳转到0x60 (死循环，测试结束)
0x60:   nop

```

测试所用的数据存储器的初值为：地址4处的32位字为```0x114514```，地址8处的32位字为```0x1919810```，其余初始化为全0。

## 测试环境路径说明

### 实验环境目录

实验四的测试环境目录如下所示。

```
D:.								您放置实验四项目的路径
│  readme.md                    说明文档
└─lab_4
    │  lab_4.xpr                实验所用的项目，直接双击即可打开完整的实验 Vivado 工程
    ├─lab4.data                 自动化测试所使用的trace文件的文件夹
    │      cpu_trace            用于对比验证的的trace文件
    │      data_data.txt        数据缓冲存储器的初始化文件
    │      inst_data.txt        指令缓冲存储器的初始化文件
    └─lab_4.srcs                所有用户编写的源码、仿真文件与约束文件，你在项目中创建的文件可以在这里找到
        ├─constrs_1
        │  └─new
        │          cpu.xdc      实验环境已经提供的约束文件
        ├─sim_1
        │  └─new
        │          cpu_tb.v     实验环境已经提供的仿真文件,需要在该文件中对源文件的顶层模块(即cpu_top.v)进行例化（对被测模块进行端口映射）
        └─sources_1
            └─new
                    cpu_top.v   实验环境已经提供的CPU自动测试环境，需要将自己设计的cpu的顶层模块(即cpu.v)在该模块中进行例化
```

### 使用绝对路径导入文件

为方便使用测试环境进行仿真及上板验证，建议使用绝对路径导入文件。

实验四需要正确设置`cpu_trace`,`data_data.txt`,`inst_data.txt`以及寄存器数据文件的路径。具体设置可参考下面两张图：

- 在`cpu_top.v`文件中，需要导入`cpu_trace`文件

```{image} ../_static/img/imgs/img7.png
:width: 100%
:align: center
```

- 导入`inst_data.txt`、`data_data.txt`等自己实现的存储器或寄存器文件，可以使用下图方法：

```{image} ../_static/img/imgs/img8.png
:width: 100%
:align: center
```

**以实验章节描述为准**
