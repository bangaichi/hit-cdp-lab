# 龙芯 Artix-7 实验板的使用

这里简单介绍一下本课程实验中如何使用提供的龙芯 Artix-7 实验板。

以下是实验板的实景图以及配套器件。

```{image} ../_static/img/labs/fpga.png
:width: 100%
:align: center
```

```{image} ../_static/img/labs/fpga_device.png
:width: 100%
:align: center
```

`<br>`在本课程实验中，我们只需用到龙芯 Artix-7 实验板、USB数据线以及电源适配器。在进行上板实验前，使用电源适配器将实验板与电源连接，并打开实验板电源开关，接着使用USB数据线将实验板与电脑连接，然后即可在Vivado中连接上实验板进行上板测试。
