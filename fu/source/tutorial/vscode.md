# VSCode 与 Verilog 编程指南

十分推荐各位同学使用 VSCode, 而不是 Vivado 2019.2 自带的文本编辑器完成本次实验.

## VSCode 安装与配置

请遵循[https://code.visualstudio.com](https://code.visualstudio.com)或是其他网站的下载安装指南.

你可以创建一个新的 VSCode 配置文件用以本次实验, 以免影响到你的其他项目.

创建新的配置文件的方法为: 左下角的设置按钮 -> 配置文件 -> 配置文件 -> 新建配置文件.

## 推荐的插件(Verilog相关)

以下三个插件选择一个即可.

1. Verilog-HDL/SystemVerilog/Bluespec SystemVerilog

```{image} ../_static/img/labs/gudience/plugin-1.png
:width: 90%
:align: center
```

   优点: 开箱即用, 功能丰富.

   缺点: 没有变量跳转和跨文件纠错

2. TerosHDL

```{image} ../_static/img/labs/gudience/plugin-2.png
:width: 90%
:align: center
```

   优点: 功能丰富, 文档齐全, 支持变量跳转和跨文件纠错. 以工程的形式管理项目(对当前实验来说可能过于复杂)

   缺点: 需要配置对应 python 环境. (如果使用虚拟环境, 请参考插件文档配置相应的python路径和必要的启动参数)

3. Digital IDE

```{image} ../_static/img/labs/gudience/plugin-3.png
:width: 90%
:align: center
```

   优点: 中文文档.

   缺点: 功能略少, bug略多.

   **注意**: 当前最新版本(0.3.3)无法使用, 需要安装 0.3.2 版本. 方法为: 打开插件安装页面, 点击 Digital IDE 插件的设置按钮, 选择"安装特定版本", 选择 0.3.2 版本.

**注意**:

1. 以上插件均需要配置 Linter 为 Vivado 的 xvlog, 具体方法可简述为: 打开插件设置, 找到 `Vivado` 或者 `xvlog` 相关 `path` 设置, 填入 Vivado 的二进制可执行文件目录路径(形如`/opt/Xilinx/Vivado/2019.2/bin/`或`C:\Xilinx\Vivado\2019.2\bin\`), 同时设置 Linter 为 `Vivado` 或 `xvlog`.
2. 不推荐使用 Verilator 作为 Linter, 因为 xvlog 和 Verilator 的语法检查规则不同, 在仿真或综合实现时可能会导致一些不必要的错误.
3. xvlog 只有在每次保存文件后才会自动检查语法错误, 并不会随写随查.

## 推荐的工作流程

1. 单独创建一个文件夹, 用于存放本次实验的所有代码源文件.

2. 使用 Vivado 添加该文件夹或文件夹中的源文件添加到工程中. (而不是使用 Vivado 创建源文件)

3. 创建 Git 仓库, 使用 Git 管理代码.

## 你可能遇到的问题

(1). **仿真不通过**

   1. 保证仿真中尽量不会出现 `X` 或者 `Z` 的信号, 否则仿真和综合的结果可能会出现不可预测的错误. (如果是 RAM 在使能信号未拉高时, RAM 读取到的数据可能是 `X`, 这是允许的)

(2). **仿真通过而上板失败**

   保证时序约束的正确性, 时序约束的正确性可以通过综合报告中的时序分析部分查看. (点击带时钟符号的 Report Timing Summary)

   Implement 的综合报告中的时序分析部分也可以查看.

```{image} ../_static/img/labs/gudience/time_report_1.png
:width: 90%
:align: center
```

   你需要做到的是保证下图的 wns 不为负数或者尽量接近 0. (wns 为 0 时, 表示所有时序约束都被满足)

```{image} ../_static/img/labs/gudience/wns.png
:width: 90%
:align: center
```

   此外, 避免使用下降沿触发的时序约束.

(3). **上板连接时无法找到设备**

   1. 安装 JTAG 驱动(Windows 会在安装 Vivado 时会默认安装驱动, 而 Linux 需要在安装 Vivado后手动安装驱动)
      1. [Linux 驱动安装方法](https://docs.amd.com/r/zh-CN/ug973-vivado-release-notes-install-license/%E5%AE%89%E8%A3%85%E7%94%B5%E7%BC%86%E9%A9%B1%E5%8A%A8?tocId=C2pTD_VRw1TwTheWRpjWCg)
   2. 确保 JTAG 线无问题或连接正确
   3. 换一块开发板试试.

(4). **综合时遇到"不支持的 BRAM" 类型**

   这是因为在你的代码中出现了形如 `reg [7:0] mem[0:255];` 的代码, 这种代码在 Vivado 2019.2 中会被识别为 BRAM.
   你需要保证你的 BRAM 的宽度和深度是 Vivado 所允许的, 也需要保证 BRAM 读写端口的数量是 Vivado 所允许的, 比如单读单写或者双端口混合读写等, 切勿出现超过 2 个读写端口的情况.

(5). 如果你还遇到了其他问题, 请尝试搜索引擎或者及时询问助教.

   1. 推荐使用 Google 和使用英文搜索, 这样会大大增加你找到答案的可能性
   2. 如果你不知道怎么登陆 Google, 那么助教也不知道
   3. 如果你害羞不敢询问, 可以 QQ 私聊助教或者发送匿名邮件至 `QA@jht213.com`. (可以备注回复邮件还是在群聊中公开回复)