# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'hit-cdp-demo'
copyright = '2023, Miya'
author = '处理器设计与实践 实验指导书'
release = '1.0.0'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = ['myst_parser', 'sphinx.ext.mathjax']

templates_path = ['_templates']
exclude_patterns = []

language = 'zh_CN'

exclude_patterns = []

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

# html_theme = 'alabaster'
html_theme = 'sphinx_rtd_theme'
html_static_path = ['_static']

# update 23/07/20
html_show_sourcelink = False
html_show_sphinx     = False

html_logo  = "./_static/logo.png"

html_theme_options = {
    'logo_only': True
}


# from recommonmark.parser import CommonMarkParser
# source_parsers = {
#                 '.md': CommonMarkParser,
#             }
# source_suffix = ['.rst', '.md']
