# Lab6：处理器下载（二选一）

## lab6-1 lab5的下载测试

### 实验目的

- 掌握 Vivado 集成开发环境
- 掌握 Verilog 语言
- 掌握 FPGA 编程方法及硬件调试手段
- 深刻理解处理器结构和计算机系统的整体工作原理

### 实验环境

- Vivado 集成开发环境
- 龙芯 Artix-7 实验平台

### 实验内容

将你实验五中设计实现的处理器下载到 Artix-7 FPGA 开发板上验证设计正确性。

### 上板测试

当所实现的CPU功能正确时，LCD屏会显示和仿真时的控制台一样的信息，如图：

```{image} ../_static/img/labs/result_lab4/result_correct.jpg
:width: 90%
:align: center
```

当上板发生错误时，LCD屏会显示错误信息，如图：

```{image} ../_static/img/labs/result_lab4/result_error.jpg
:width: 90%
:align: center
```

注意：

- 尽管上板输出的信息和仿真时的控制台是一样的，但这并不代表仿真成功就一定能够在上板时得到相同的正确结果（尽管这属于小概率事件）。请确保你的verilog代码**确实是在描述硬件逻辑，而不是在描述仿真行为**。在仿真时，请确保你模块接口及内部信号的波形中不会出现Z或X。再次提醒：**避免使用下降沿触发**。
- 上板前需要对整个项目进行综合（Synthesis）、实现（Implementation）、比特流生成（Generate Bitstream），你可以将它们理解为特殊的“编译”行为。其中综合步骤需要综合项目中所有使用的IP核，这些IP核只需要在最开始时被综合一次，即不需要参与后续的所有综合过程。实验提供的vivado项目中自带的少数IP核在某些特定机器上综合时间较长（可能达20~30min），**请务必耐心等待**。后续的没有IP核参与的综合-实现-比特流生成过程一般用时4\~5min。

### 上板在线调试

上板测试时，你需要使用 vivado 提供的上板在线调试功能，抓取你的CPU内部的某些信号，将波形图记录入实验报告。当然，如果你的CPU在上板时发生错误，你也可以使用在线调试来发现问题并解决。在实现这一部分前，**请务必仔细阅读实验网站提供的 vivado 使用说明中“FPGA 在线调试说明”一节**。

这里提供一个简单的示例，例如我们可以将CPU输出的```debug_wb_*```信号，以及CPU内部的```PC```与正在处理的指令字，使用```(*mark_debug = "true"*)```进行标记：

```verilog
    // ...

    output          data_sram_en,
    output[3:0]     data_sram_wen,
    output[31:0]    data_sram_addr,
    output[31:0]    data_sram_wdata,
    input[31:0]     data_sram_rdata,

    output[31:0]    debug_wb_pc,
    (*mark_debug = "true"*)output          debug_wb_rf_wen,
    (*mark_debug = "true"*)output[4:0]     debug_wb_rf_wnum,
    (*mark_debug = "true"*)output[31:0]    debug_wb_rf_wdata
);

    // ...

    (*mark_debug = "true"*)reg[31:0]       pc;
    (*mark_debug = "true"*)wire[31:0]      inst = inst_sram_rdata;

    // ...
```

按照“FPGA 在线调试说明”一节中的步骤，在将项目综合后进行 Set Up Debug，然后生成比特流上板，设置在```debug_wb_rf_wen=1```时进行信号抓取，最后会得到这样的波形图：

```{image} ../_static/img/labs/lab5/intro.png
:width: 90%
:align: center
```

在撰写实验报告时，你需要自行选择测试用例中的一条指令，在在线调试时设置：当执行到这条指令时进行信号抓取（例如将```PC```的值作为条件）。你可以自行选择CPU内部的信号（不需要很多，够用即可）用作上板调试信号，在上板的情况下用它们的波形图简要展示这条指令是如何执行的。

### 抓取debug信号

你需要详细阅读 **PRE-3.Vivado使用说明-3.2.6.FPGA 在线调式说明**，完成以下任务：

1. 根据仿真波形，找到测试结束时的指令地址

2. 给 `pc` 寄存器和其他你认为需要的信号添加 debug 标志。

3. 添加抓取条件：当 `pc` 寄存器的值为上述值时。

4. 在实验报告中说明CPU此时正在执行什么指令。

## lab6-2 la32r处理器的实现

### 实验目的

- 掌握 Vivado 集成开发环境
- 掌握 Verilog 语言
- 掌握 FPGA 编程方法及硬件调试手段
- 掌握la32r指令集的20条基础指令

### 实验环境

- Vivado 集成开发环境
- 龙芯 Artix-7 实验平台

### 实验内容

1. 阅读并理解`lab6/myCPU/`目录下提供的代码。
2. 完成代码调试。我们提供的代码中加入了若干错误，请大家通过仿真波形的调试修复这些错误，使设计可以通过仿真和上板验证。

提示：不仅仅是`mycpu_top.v`文件，每一个 `.v` 文件都可能存在错误。

#### 指令系统定义

本实验要求实现la32r指令集中的20条基础指令。

`add.w` `sub.w` `slt` `sltu` `nor` `and` `or` `xor` `slli_w` `srli_w`
`srai_w` `addi_w` `ld_w` `st_w` `jirl` `b` `bl` `beq` `bne` `lu12i_w`

```{image} ../_static/img/labs/lab5/ins_sys.png
:width: 90%
:align: center
```

完整的指令集手册可以在这里找到：[https://soc.ustc.edu.cn/COD/lab1/src/LA32R_ref.pdf](https://soc.ustc.edu.cn/COD/lab1/src/LA32R_ref.pdf)

#### 接口及控制信号

你设计的la32r处理器应该符合这样的接口：

| 名称              | 宽度 | 方向 | 描述                   |
| ----------------- | ---- | ---- | ---------------------- |
| clk               | 1    | IN   | 时钟信号               |
| resetn            | 1    | OUT  | 重置信号               |
| inst_sram_we      | 1    | OUT  | 指令RAM写使能          |
| inst_sram_addr    | 32   | OUT  | 指令RAM读写地址        |
| inst_sram_wdata   | 32   | OUT  | 指令RAM写数据          |
| inst_sram_rdata   | 32   | OUT  | 指令RAM读数据          |
| data_sram_we      | 1    | OUT  | 数据RAM写使能          |
| data_sram_addr    | 32   | OUT  | 数据RAM读写地址        |
| data_sram_wdata   | 32   | OUT  | 数据RAM写数据          |
| data_sram_rdata   | 32   | OUT  | 数据RAM读数据          |
| debug_wb_pc       | 32   | OUT  | 写回阶段的PC           |
| debug_wb_rf_we    | 4    | OUT  | 写回阶段的寄存器写使能 |
| debug_wb_rf_wnum  | 5    | OUT  | 写回阶段的写寄存器号   |
| debug_wb_rf_wdata | 32   | OUT  | 写回阶段的写数据       |

#### 创建工程目录（非常重要，必须依照以下方法创建工程）

1.打开 Vivado，停留在初始界面，找到最下面的 **`TCL CONSOLE`**。如果不是这样的界面，那就点击最下面 **`TCL CONSOLE`** 的框。

```{image} ../_static/img/labs/lab5/tcl_console.png
:width: 90%
:align: center
```

2.找到 `lab6\soc_verify\soc_dram\run_vivado` 目录，在资源管理器的地址框中复制**绝对路径**。

```{image} ../_static/img/labs/lab5/path.png
:width: 90%
:align: center
```

3.输入 `cd` 到 Vivado 中 `TCL CONSOLE` 的输入框中, 再粘贴第 2 步中的绝对路径。  
**注意！Vivado 中路径需要`/`而不是Windows默认的`\`。一个便捷的方法是在粘贴后删除末尾的字母，然后会自动提示可补全的路径，这个时候按下TAB即可自动补全，同时`\`全部变为`/`**

```{image} ../_static/img/labs/lab5/cd.png
:width: 90%
:align: center
```

4.输入 `source ./create_project.tcl`，按下回车执行。等待工程创建完毕。

```{image} ../_static/img/labs/lab5/create.png
:width: 90%
:align: center
```

### 实验要求
#### 实验预习

- 掌握CPU的工作原理，了解实验开发所需的软硬件平台。
- 掌握20种指令运算操作的意义，完成这20种指令的编码。

#### 完成实验内容

按照实验内容完成CPU的设计与实现，在**仿真**和**上板**两种环境下通过本实验所提供的自动测试环境，记录运行过程和结果，完成实验报告。

### 自动测试环境

要求使用我们提供的自动测试环境对CPU进行测试，包括仿真测试和上板测试两部分。

#### 使用要求

要求每个同学的CPU对外暴露以下接口（信号名称与位数均不可改变）：

```verilog
module mycpu_top(
    input  wire        clk,
    input  wire        resetn,
    // inst sram interface
    output wire        inst_sram_we,
    output wire [31:0] inst_sram_addr,
    output wire [31:0] inst_sram_wdata,
    input  wire [31:0] inst_sram_rdata,
    // data sram interface
    output wire        data_sram_we,
    output wire [31:0] data_sram_addr,
    output wire [31:0] data_sram_wdata,
    input  wire [31:0] data_sram_rdata,
    // trace debug interface
    output wire [31:0] debug_wb_pc,
    output wire [ 3:0] debug_wb_rf_we,
    output wire [ 4:0] debug_wb_rf_wnum,
    output wire [31:0] debug_wb_rf_wdata
);
// TODO

endmodule
```

注意：

- 这是一个单周期CPU，你实现的```mycpu_top```模块中大部分信号为**组合逻辑**，只有少数信号在实现时使用阻塞赋值等用于时序逻辑的写法。

#### 测试用例

在 `func/obj/test.s` 中可查看。

当你发现仿真出现错误时，一个很好的办法就是打开这个文件，找到出错指令的位置，查看什么指令发生了错误，以及对应的上下文。

此外你还需要知道，如果是跳转或者访存指令发生错误，那么真正发生错误的指令很可能在更早的某条指令。至于为什么会这样，你可以从我们的**对比纠错机制**出发，来思考这个问题。

#### 仿真

在进行仿真时，若你实现的CPU功能正确，自动测试环境会在控制台打印 **```PASS```**

若CPU对某条输入的指令执行错误，自动测试环境会在控制台打印错误信息，错误信息包含你的处理器在哪里出现了错误以及正确信息是什么。

```{image} ../_static/img/labs/lab5/console_result.png
:width: 90%
:align: center
```

#### 上板测试

你实现的CPU通过仿真验证后，需要将设计下载到 Artix-7 FPGA 开发板上验证设计正确性。当所实现的CPU功能正确时，灯管和数码管会显示相应的信息。

在FPGA上板验证时其结果正确与否的判断也只有一种方法，func正确的执行行为是：

1. 开始，单色LED全灭，双色LED灯一红一绿，数码管显示全0；
2. 执行过程中，单色LED全灭，双色LED灯一红一绿，数码管高8位和低8位同步累加；
3. 结束时，单色LED全灭，双色LED灯亮两绿，数码管高8位和低8位数值相同，对应测试功能点数目。
4. 如果func执行过程中出错了，则数码管高8位和低8位第一次不同处即为测试出错的功能点编号，且最后的结果是单色LED全亮，双色LED灯亮两红，数码管高8位和低8位数值不同。

```{image} ../_static/img/labs/lab5/fpga.png
:width: 90%
:align: center
```
