# Lab3：内存与寄存器堆

## 实验目的

- 熟悉并掌握 MIPS 计算机中寄存器堆的原理和设计方法
- 理解源操作数/目的操作数的概念
- 理解 RAM 读取、写入数据的过程
- 掌握调用 xilinx 库 IP 实例化 RAM 的设计方法

## 实验环境

- vivado 集成开发环境

## 实验内容

### 寄存器堆

学习 MIPS 计算机中寄存器堆的设计原理。实验要求设计寄存器堆，包含1个写端口和2个读端口。

### RAM

学习 RAM 的读写时序，并定制一块 RAM IP 核，在顶层文件中实例化。实验要求实现同步 RAM，定制深度为65536，定制宽度为32。

### 模块接口设计

寄存器堆的信号说明如下。

| 名称   | 宽度 | 方向 | 描述                |
| ------ | ---- | ---- | ------------------- |
| clk    | 1    | IN   | 时钟信号            |
| raddr1 | 5    | IN   | 寄存器堆读地址1     |
| rdata1 | 32   | OUT  | 寄存器堆读返回数据1 |
| raddr2 | 5    | IN   | 寄存器堆读地址2     |
| rdata2 | 32   | OUT  | 寄存器堆读返回数据2 |
| we     | 1    | IN   | 寄存器堆写使能      |
| waddr  | 5    | IN   | 寄存器堆写地址      |
| wdata  | 32   | IN   | 寄存器堆写数据      |

RAM 顶层模块的接口信号说明如下。

| 名称      | 宽度 | 方向 | 描述                              |
| --------- | ---- | ---- | --------------------------------- |
| clk       | 1    | IN   | 时钟信号                          |
| ram_wen   | 1    | IN   | 同步 RAM 写使能，置位时写入       |
| ram_addr  | 16   | IN   | 同步 RAM 地址信号，表示读/写地址  |
| ram_wdata | 32   | IN   | 同步 RAM 写数据信号，表示写入数据 |
| ram_rdata | 32   | OUT  | 同步 RAM 读数据信号，表示读出数据 |

## 实验要求

### 实验预习

- 掌握 MIPS 寄存器堆的工作原理
- 根据给定的输入输出端口设计寄存器堆
- 掌握存储器的工作原理，理解同步 RAM 与异步 RAM 的区别。
- 掌握使用 xilinx 库 IP 进行设计的方法。

### 完成实验内容

* 按照实验内容完成寄存器堆的设计，自行编写测试用例，给出模拟测试波形，记录运行过程和结果，完成实验报告。
* 按照实验内容实例化同步 RAM IP，自行设计测试用例，给出模拟测试波形，观察时序特征，对比读写行为的异同，完成实验报告。

## 实验帮助信息

### regfile

regfile.v 的正确实现对于后续实现 CPU 十分重要，力求简洁。实现中可以采用寄存器数组的形式进行描述。

### RAM IP

```{note}
* 该部分实验内容体现在报告中即可。

```

新建工程，在 PROJECT MANAGER 中点击 IP Catalog，打开 IP 目录。

```{image} ../_static/img/labs/ip_23_0.png
:width: 90%
:align: center
```
<br>

在搜索框中查找 `Block Memory Generator`。

```{image} ../_static/img/labs/ip_23_1.png
:width: 90%
:align: center
```
<br>

在 Basic 选项卡下，将IP重命名为 block_ram，内存类型设置为 `Single Port RAM`。注意不要勾选 `Byte Write Enable `。

```{image} ../_static/img/labs/ip_23_2.png
:width: 90%
:align: center
```
<br>

在 Port A Options 选项卡下，设置 RAM 深度及宽度。使能端口设置为 `Always Enabled`，不要勾选 `Primitives Output Register`，输出不需要打拍。

```{image} ../_static/img/labs/ip_23_3.png
:width: 90%
:align: center
```
<br>

其他设置保持默认即可。


```{note}
* vivado提供两种类型的RAM IP - BRAM 和 DRAM（Distributed RAM）。在综合实现后的资源占用报告中，能够观察到LUT RAM一项，它就是DRAM/ROM及移位寄存器占用资源的总和。

* 若实现异步RAM，请自行尝试实例化DRAM IP，并与BRAM比较，观察不同时序下的波形特征。
```

bram_top.v 作为同步 RAM 的源码顶层文件，其代码如下。

```verilog
module ram_top (
    input         clk      ,
    input  [15:0] ram_addr ,
    input  [31:0] ram_wdata,
    input         ram_wen  ,
    output [31:0] ram_rdata
);
					   
block_ram block_ram (
    .clka (clk       ),
    .wea  (ram_wen   ),
    .addra(ram_addr  ),
    .dina (ram_wdata ),
    .douta(ram_rdata ) 
);
endmodule
```

### 尝试数据读写

完成基本的 IP 例化实验后，尝试将 RAM 中的数据读入寄存器，或将寄存器中的数据写回 RAM。可以在中间加入实验二实现的 ALU，模拟对数据进行一些操作。


## 选做：指令 ROM

在后续的 CPU 实验中，我们将使用 BRAM 实现指令 ROM。仍然调用 ram_top，将写使能恒置为0，并加载 .coe 格式的初始化文件。

在 inst_ram.txt 前加入两行：

```
memory_initialization_radix = 16;
memory_initialization_vector =
```

并修改文件后缀为 .coe，装载入 BRAM IP核。在 Other Options 选项卡下，勾选 `Load Init File`，并填入 .coe 文件路径。

```{image} ../_static/img/labs/ip_23_4.png
:width: 90%
:align: center
```
<br>
