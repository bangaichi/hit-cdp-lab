.. hit-cdp-demo documentation master file, created by
   sphinx-quickstart on Wed May 31 17:26:47 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.
   Indices and tables
   ==================

   * :ref:`genindex`
   * :ref:`modindex`
   * :ref:`search`


处理器设计与实践（2024秋）
========================================

Welcome to hit-cdp-lab's doc for 计算学部!
========================================

========================================

.. used to get the title of TOC right in generated pdf
.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: 目录

.. toctree::
   :maxdepth: 1
   :numbered: 3
   :caption: Intro:

   intro/intro.md
  
.. toctree::
   :maxdepth: 1
   :numbered: 3
   :caption: Pre:

   tutorial/verilog
   tutorial/dev_env
   tutorial/dev_step
   tutorial/vscode

.. toctree::
   :maxdepth: 1
   :numbered: 3
   :caption: Labs:
   
   testcase/lab0.md
   testcase/lab1.md
   testcase/lab2.md
   testcase/lab3.md
   testcase/lab4.md
   testcase/lab5.md

.. toctree::
   :maxdepth: 1
   :numbered: 3
   :caption: Appendix:
   
   appendix/vivado_intro.md
